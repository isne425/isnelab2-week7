#include<iostream>
#include "tree.h"
#include<ctime>
#include<vector>

using namespace std;

void main() 
{
	srand(time(0));
	Tree<int> mytree;			// unbalanced tree
	Tree<int> balancetree;

	for (int i = 0;i < 500;i++)
	{
		int num = rand();
		mytree.insert(num);		
	}
	cout << "Original tree's height = " << mytree.height() << endl;
	
	// Balancing
	mytree.inorder();
	balancetree.balancing(mytree.arr, 0, 499);
	cout << "Balanced tree's height = " << balancetree.height() << endl;
	cout << "\n\n\n";
	mytree.showinorder();
	cout << "\n\n\n";

	//UI
	int searchNum;
	cout << "Search Original : ";
	cin >> searchNum;
	if (balancetree.search(searchNum)) 
	{
		cout << "Found!";
	}
	else {
		cout << "Sorry,it's absent!";
	}
	int deleteNum;
	cout << "Input a number to delete:";
	cin >> deleteNum;
	mytree.searchfordelete(deleteNum);
	


	system("pause");
}