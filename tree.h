#include <algorithm>

using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node {
public:
	Node() { left = right = NULL; }
	Node(const T& el, Node *l = 0, Node *r = 0) 
	{
		key = el; left = l; right = r;
	}
	T key;
	Node *left;
	Node *right;

};

template<class T>
class Tree {
public:
	Tree() { root = 0; }
	Tree(int a) {
		root = 0;
		arr = new int[a];
		index = 0;
	}
	~Tree() { clear(); }
	void clear() { clear(root); root = 0; }
	bool isEmpty() { return root == 0; }
	void inorder() { inorder(root); }
	void insert(const T& el);
	int height() { return height(root); }
	void balancing(T data[], int first, int last);
	void showinorder() { showinorder(root); }
	bool search(const T& search);
	void deleteNode(Node<T> *& node);
	bool searchfordelete(const T &search);
	 
	int *arr;

protected:
	Node<T> *root;
	int height(Node<T> *p);
	void showinorder(Node<T> *p);
	void clear(Node<T> *p);
	void inorder(Node<T> *p);
	int index;
	int h; // height
	
};

template<class T>
void Tree<T>::clear(Node<T> *p)
{
	if (p != 0) {
		clear(p->left);
		clear(p->right);
		delete p;
	}
}

template<class T>
void Tree<T>::inorder(Node<T> *p) {
	/*if (p == 0) return;
	inorder(p->left);
	arr[index] = p->key;
	index++;
	inorder(p->right);*/
	if (root != NULL)
	{
		if (p->left != NULL) {
			inorder(p->left);
		}
		cout << p->key << " " << endl;
		if (p->right != NULL) {
			inorder(p->right);
		}
	}
	else if (root == NULL) {
		cout << " Empty List " << endl;
	}

}

template<class T>
void Tree<T>::insert(const T &el) {
	Node<T> *p = root, *prev = 0;
	while (p != 0) {
		prev = p;
		if (p->key < el)
			p = p->right;
		else
			p = p->left;
	}
	if (root == 0)
		root = new Node<T>(el);
	else if (prev->key<el)
		prev->right = new Node<T>(el);
	else
		prev->left = new Node<T>(el);
}

template<class T>
int Tree<T>::height(Node<T> *p) {
	if (p != 0)
	{
		int hleft = height(p->left);
		int hright = height(p->right);
		h = max(hleft, hright) + 1;
		return h;
	}
	return 0;
}

template<class T>
void Tree<T>::balancing(T data[], int first, int last) {
	if (first <= last) {
		int middle = (first + last) / 2;			// find median
		insert(data[middle]);						// root = median
		cout << data[middle] << " ";
		balancing(data, first, middle - 1);
		balancing(data, middle + 1, last);
	}
}

template<class T>
void Tree<T>::showinorder(Node<T> *p) {

	if (root != NULL)
	{
		if (p->left != NULL) {
			showinorder(p->left);
		}
		cout << p->key << " " << endl;
		if (p->right != NULL) {
			showinorder(p->right);

		}
	}
	else if (root == NULL) {
		cout << " Empty List " << endl;
	}
}

template<class T>
bool Tree<T>::search(const T &search) 
{
	Node<T> *p = root;
	while (p != NULL) 
	{
		if (p->key == search) 
		{
			return true;
		}
		//go to left tree
		else if (p->key > search) 
		{
			p = p->left;
		}
		//else go to right tree
		else 
		{
			p = p->right;
		}
	}
	//not found
	return false;
}

template<class T>
void Tree<T>::deleteNode(Node<T> *&node) {
	Node<T> *prev, *tmp = node;
	if (node->right == 0)
		node = node->left;
	else if (node->left == 0)
		node = node->right;
	else {
		tmp = node->left;
		prev = node;
		while (tmp->right != 0) {
			prev = tmp;
			tmp = tmp->right;
		}
		node->key = tmp->key;
		if (prev == node)
			prev->left = tmp->left;
		else prev->right = tmp->left;
	}
	delete tmp;
}


template<class T>
bool Tree<T>::searchfordelete(const T &search) {
	Node<T> *p = root;

	while (p != NULL) {

		if (p->key == search) {
			deleteNode(p);
			cout << "delete !!" << endl;
			return true;
		}
		//go to left tree
		else if (p->key > search) {
			p = p->left;
		}//else go to right tree
		else {
			p = p->right;
		}
	}
	cout << "Haven't it !!";
	//not found
	return false;
}
#endif